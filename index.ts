/**
 * FatCat Coders tank battle challenge :)
 * 
 * There will be 5 tanks, representing different
 * programming langugages.
 * 
 * May the best language live
 * 
 * KAPOW!!!
 */

import { buildTank, battleItOut } from "./lib/Tank";

const tankPrototypes = [
  "Rust",
  "JavaScript",
  "TypeScript",
  "Haskell",
  "HTML", // If it wins, it's a programming language
];
const initialTanks = tankPrototypes.map(buildTank);

const winner = battleItOut(initialTanks);

console.log(winner);
