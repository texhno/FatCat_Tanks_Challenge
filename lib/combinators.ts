// @ts-nocheck

export const tap = (fn) => (input) => {
  fn(input);
  return input;
};

export const doIf = (fn, pred) => (input) =>
  pred ? fn(input) : input;
