// @ts-nocheck

export const pipe = (...fns) => (input) =>
  fns.reduce((acc, fn) => fn(acc), input);

export const mapR = (fn) => (input) =>
  input.reduce((acc, x) => [...acc, fn(x)], []);

export const filterR = (pred) => (input) =>
  input.reduce((acc, x) => pred(x) ? [...acc, x] : acc ,[]);
