import { ITank } from "./Tank";
import { filterR, pipe } from "./reducers";

type CritMultiplier = (health: number) => number;
export const critMultiplier: CritMultiplier = (health) =>
  Math.floor(Math.random() * 10) >= 10 - health / 10 ? 1 : 2;

type PickRandom = <T>(arr: T[]) => T;
export const pickRandom: PickRandom = (arr) =>
  arr[Math.floor(Math.random() * arr.length)];

// @ts-ignore
export const setProp = (prop, val) => (obj) => obj[prop] = val;

type IsLive = (tank: ITank) => boolean;
export const isLive: IsLive = (tank) => tank.health >= 0;

type IsOther = (name: string) => (tank: ITank) => boolean;
export const isOther: IsOther = (name) => (tank) => tank.name !== name;

export const isReadyToAttack = (tank: ITank) => tank.attackDelay === 0;

type DealDamageToRandom = (tanks: ITank[]) => (tank: ITank) => void;
export const dealDamageToRandom: DealDamageToRandom = (tanks) => (tank) => {
    console.log("hp");
    const target = pipe(filterR(isOther(tank.name)), pickRandom)(tanks);
    const attackDamage = critMultiplier(tank.health) * tank.health / 100;
    setProp("health", target.health - attackDamage)(target);
};
